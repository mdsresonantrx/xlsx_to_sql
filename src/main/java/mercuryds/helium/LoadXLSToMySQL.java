package mercuryds.helium;

import com.google.inject.Guice;
import com.google.inject.Inject;
import mercuryds.helium.processfiles.ExcelSheetToDataMapper;
import mercuryds.helium.tableloader.TableToSQLWriter;

import java.io.InputStream;
import java.util.List;

/**
 * Created by bachman on 12/20/2017.
 */
public class LoadXLSToMySQL {
    private final List<InputStream> excelSheetInputStreams;
    private final ExcelSheetToDataMapper excelSheetToDataMapper;
    private final TableToSQLWriter tableToSQLWriter;

    @Inject
    public LoadXLSToMySQL(List<InputStream> excelSheetInputStreams, ExcelSheetToDataMapper excelSheetToDataMapper, TableToSQLWriter tableToSQLWriter) {
        this.excelSheetInputStreams = excelSheetInputStreams;
        this.excelSheetToDataMapper = excelSheetToDataMapper;
        this.tableToSQLWriter = tableToSQLWriter;
    }

    public static void main(String[] args) {
        Guice.createInjector(new LoadXLSToMySQLModule()).getInstance(LoadXLSToMySQL.class).run();
    }

    private void run() {
        excelSheetInputStreams.stream()
                .map(excelSheetToDataMapper::map)
                .flatMap(List::stream)
                .forEach(tableToSQLWriter::write);
    }
}
