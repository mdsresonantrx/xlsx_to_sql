package mercuryds.helium;

import com.google.inject.PrivateModule;
import mercuryds.helium.config.HoconToGuiceModule;
import mercuryds.helium.loadfiles.FileFinderModule;
import mercuryds.helium.loadfiles.FileListToStreamListModule;
import mercuryds.helium.tableloader.JOOQConnectionModule;
import mercuryds.helium.typechecker.TypeCheckerModule;

/**
 * Created by bachman on 12/20/2017.
 */
public class LoadXLSToMySQLModule extends PrivateModule {
    protected void configure() {
        install(new HoconToGuiceModule());
        install(new FileFinderModule());
        install(new FileListToStreamListModule());
        install(new TypeCheckerModule());
        install(new JOOQConnectionModule());
        bind(LoadXLSToMySQL.class);
        expose(LoadXLSToMySQL.class);
    }
}
