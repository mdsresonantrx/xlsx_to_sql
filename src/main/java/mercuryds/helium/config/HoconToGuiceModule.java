package mercuryds.helium.config;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * Created by bachman on 12/21/2017.
 */
public class HoconToGuiceModule extends AbstractModule {
    @Override
    protected void configure() {
        Config load = ConfigFactory.load();
        load.entrySet().forEach(entry -> bindConstant()
                .annotatedWith(Names.named(entry.getKey()))
                .to(entry.getValue().unwrapped().toString()));
    }
}
