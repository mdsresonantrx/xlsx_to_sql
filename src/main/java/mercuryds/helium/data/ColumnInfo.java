package mercuryds.helium.data;

import lombok.Builder;
import lombok.Value;

/**
 * Created by bachman on 12/21/2017.
 */
@Value
@Builder
public class ColumnInfo {
    String properties;
    org.jooq.DataType jooqType;
}
