package mercuryds.helium.data;

import lombok.Builder;
import lombok.Value;

import java.util.List;

/**
 * Created by bachman on 12/20/2017.
 */
@Value
@Builder
public class TableData {
    String tableName;
    List<TableHeader> headers;
    List<List<Object>> data;
    List<ColumnInfo> columnInfo;
}
