package mercuryds.helium.data;

import lombok.Builder;
import lombok.Value;

/**
 * Created by bachman on 12/20/2017.
 */
@Value
@Builder
public class TableHeader {
    String title;
}
