package mercuryds.helium.loadfiles;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by bachman on 12/21/2017.
 */
public class FileFinder {
    public List<File> findFiles(File rootDir, Pattern pattern) {
        File[] subdirectories = rootDir.listFiles(File::isDirectory);
        File[] matchesInThisDir = rootDir.listFiles(file -> !file.isDirectory() && pattern.matcher(file.getName()).matches());

        return Stream.concat
                (
                        Arrays.stream(subdirectories)
                                .map(subdir -> this.findFiles(subdir, pattern))
                                .flatMap(Collection::stream),
                        Stream.of(matchesInThisDir)
                )
                .collect(Collectors.toList());
    }
}
