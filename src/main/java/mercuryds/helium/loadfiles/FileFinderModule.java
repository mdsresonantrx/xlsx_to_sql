package mercuryds.helium.loadfiles;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by bachman on 12/21/2017.
 */
@Slf4j
public class FileFinderModule implements Module {
    @Override
    public void configure(Binder binder) {

    }

    @Provides
    List<File> getFiles(@Named("filefinder.rootdir") String rootdir,
                        @Named("filefinder.pattern") String pattern,
                        FileFinder fileFinder) {
        List<File> files = fileFinder.findFiles(new File(rootdir), Pattern.compile(pattern));
        log.info("loading {} files", files.size());
        return files;
    }
}
