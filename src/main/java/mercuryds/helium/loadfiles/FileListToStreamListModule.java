package mercuryds.helium.loadfiles;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by bachman on 12/21/2017.
 */
@Slf4j
public class FileListToStreamListModule extends AbstractModule {
    @Provides
    List<InputStream> inputStreamGetter(List<File> files) {
        return files.stream()
                .map(file -> {
                    try {
                        return new FileInputStream(file);
                    } catch (FileNotFoundException e) {
                        log.warn("Error loading file {}, skipping", file);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    protected void configure() {

    }
}
