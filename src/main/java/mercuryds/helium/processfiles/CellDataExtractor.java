package mercuryds.helium.processfiles;

import com.google.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;

import static org.apache.poi.ss.usermodel.Cell.CELL_TYPE_BLANK;

@Slf4j
public class CellDataExtractor {
    private final CellValueExtractor cellValueExtractor;

    @Inject
    public CellDataExtractor(CellValueExtractor cellValueExtractor) {
        this.cellValueExtractor = cellValueExtractor;
    }

    public Object extract(XSSFCell xssfCell) {
        switch(xssfCell.getCellType()) {
            case Cell.CELL_TYPE_NUMERIC:
                return xssfCell.getNumericCellValue();
            case Cell.CELL_TYPE_STRING:
                return xssfCell.getStringCellValue();
            case Cell.CELL_TYPE_FORMULA:
                try {
                    log.debug("evaluating " + xssfCell.getCellFormula());
                    XSSFFormulaEvaluator formulaEvaluator = xssfCell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
                    CellValue evaluate = formulaEvaluator.evaluate(xssfCell);
                    return cellValueExtractor.extract(evaluate);
                } catch(Exception e) {
                    log.error("Formula evaluation error", e);
                    return "formula_evalutation_error";
                }
            case CELL_TYPE_BLANK:
                return "";
            default:
                throw new RuntimeException("unrecognized type: " + xssfCell.getCellType());
        }
    }

}
