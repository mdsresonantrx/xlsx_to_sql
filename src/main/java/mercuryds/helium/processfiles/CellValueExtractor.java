package mercuryds.helium.processfiles;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;

public class CellValueExtractor {
    public Object extract(CellValue cellValue) {
        switch (cellValue.getCellType()) {
            case Cell.CELL_TYPE_BOOLEAN:
                return cellValue.getBooleanValue();
            case Cell.CELL_TYPE_NUMERIC:
                return cellValue.getNumberValue();
            case Cell.CELL_TYPE_STRING:
                return cellValue.getStringValue();
            default:
                throw new RuntimeException("cellvalue extractor unrecognized type: " + cellValue.getCellType());
        }
    }
}
