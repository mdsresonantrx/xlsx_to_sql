package mercuryds.helium.processfiles;

import com.google.inject.Inject;
import mercuryds.helium.data.ColumnInfo;
import mercuryds.helium.typechecker.TypeChecker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by bachman on 12/21/2017.
 */
public class ColumnInfoFromDataExtractor {
    private final Set<TypeChecker> typeCheckers;

    @Inject
    public ColumnInfoFromDataExtractor(Set<TypeChecker> typeCheckers) {
        this.typeCheckers = typeCheckers;
    }

    public List<ColumnInfo> extract(List<List<Object>> data) {
        if (data.size() == 0 || data.get(0).size() == 0)
            return Collections.emptyList();


        return IntStream.range(0, data.get(0).size())
                .mapToObj(column -> {
                            List<TypeChecker> toKeep = new ArrayList<>(this.typeCheckers);
                            IntStream.range(0, data.size())
                                    .forEach(row -> {
                                        typeCheckers.stream()
                                                .filter(toKeep::contains)
                                                .filter(checker -> !checker.allows(data.get(row).get(column)))
                                                .forEach(toKeep::remove);
                                    });
                            if (toKeep.size() == 0)
                                throw new RuntimeException("There was no fallback typecheker");
                            return toKeep.get(0);
                        }
                ).map(TypeChecker::getColumnInfo)
                .collect(Collectors.toList());
    }
}
