package mercuryds.helium.processfiles;

import com.google.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import mercuryds.helium.data.TableData;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by bachman on 12/20/2017.
 */
@Slf4j
public class ExcelSheetToDataMapper {
    private final WorkBookToTableConverter workbookToTableConverter;

    @Inject
    public ExcelSheetToDataMapper(WorkBookToTableConverter workbookToTableConverter) {
        this.workbookToTableConverter = workbookToTableConverter;
    }

    public List<TableData> map(InputStream inputStream) {
        XSSFWorkbook myWorkBook = null;
        try {
            myWorkBook = new XSSFWorkbook(inputStream);
        } catch (IOException e) {
            log.warn("Error loading workbook", e);
            return Collections.emptyList();
        }

        return IntStream.range(0, myWorkBook.getNumberOfSheets())
                .mapToObj(myWorkBook::getSheetAt)
                .map(workbookToTableConverter::convert)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }
}
