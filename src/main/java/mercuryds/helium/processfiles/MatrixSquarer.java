package mercuryds.helium.processfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class MatrixSquarer {
    public List<List<Object>> square(List<List<Object>> data) {
        int columns = data.stream().filter(Objects::nonNull).mapToInt(List::size).max().orElseThrow(RuntimeException::new);

        return data.stream()
                .map(list -> {
                    if (list.size() == columns)
                        return list;
                    else {
                        List<Object> newlist = new ArrayList<>(list);
                        for (int x = 0; x < columns - list.size(); x++) {
                            newlist.add(null);
                        }
                        return newlist;
                    }
                })
                .collect(Collectors.toList());
    }
}
