package mercuryds.helium.processfiles;

import com.google.inject.Inject;
import org.apache.poi.xssf.usermodel.XSSFRow;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by bachman on 12/21/2017.
 */
public class RowDataExtractor {
    private final CellDataExtractor cellDataExtractor;

    @Inject
    public RowDataExtractor(CellDataExtractor cellDataExtractor) {
        this.cellDataExtractor = cellDataExtractor;
    }

    public List<Object> extract(XSSFRow cells) {
        return IntStream.range(0, cells.getLastCellNum() + 1)
                .mapToObj(cells::getCell)
                .map(Optional::ofNullable)
                .map(val -> val.map(cellDataExtractor::extract))
                .map(val -> val.orElse(null))
                .collect(Collectors.toList());

    }
}
