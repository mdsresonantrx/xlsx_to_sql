package mercuryds.helium.processfiles;

import com.google.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import mercuryds.helium.data.ColumnInfo;
import mercuryds.helium.data.TableData;
import mercuryds.helium.data.TableHeader;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by bachman on 12/20/2017.
 */
@Slf4j
public class WorkBookToTableConverter {
    private final XSSFRowToHeaderConverter convertXSSFRowToHeader;
    private final RowDataExtractor rowDataExtractor;
    private final ColumnInfoFromDataExtractor columnInfoFromDataExtractor;
    private final MatrixSquarer matrixSquarer;

    @Inject
    public WorkBookToTableConverter(XSSFRowToHeaderConverter convertXSSFRowToHeader, RowDataExtractor rowDataExtractor, ColumnInfoFromDataExtractor columnInfoFromDataExtractor, MatrixSquarer matrixSquarer) {
        this.convertXSSFRowToHeader = convertXSSFRowToHeader;
        this.rowDataExtractor = rowDataExtractor;
        this.columnInfoFromDataExtractor = columnInfoFromDataExtractor;
        this.matrixSquarer = matrixSquarer;
    }

    public Optional<TableData> convert(XSSFSheet rows) {
        Iterator rowIter = rows.rowIterator();
        if (!rowIter.hasNext()) {
            log.warn("found a sheet with no rows {}", rows);
            return Optional.empty();
        }

        XSSFRow headersrow = (XSSFRow) rowIter.next();
        List<TableHeader> headers = convertXSSFRowToHeader.convert(headersrow);

        if (!rowIter.hasNext()) {
            log.warn("found a sheet with no data rows {}", rows);
            return Optional.empty();
        }

        List<List<Object>> data = IntStream.range(rows.getFirstRowNum() + 1, rows.getLastRowNum() + 1)
                .mapToObj(rows::getRow)
                .map(rowDataExtractor::extract)
                .collect(Collectors.toList());

        data = matrixSquarer.square(data);

        List<ColumnInfo> extract = columnInfoFromDataExtractor.extract(data);

        return Optional.of(TableData.builder()
                .tableName(rows.getSheetName())
                .headers(headers)
                .data(data)
                .columnInfo(extract)
                .build());
    }
}
