package mercuryds.helium.processfiles;

import com.google.inject.Inject;
import mercuryds.helium.data.TableHeader;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by bachman on 12/20/2017.
 */
public class XSSFRowToHeaderConverter {
    private final SqlColumnNameSanitizer sqlColumnNameSanitizer;

    @Inject
    public XSSFRowToHeaderConverter(SqlColumnNameSanitizer sqlColumnNameSanitizer) {
        this.sqlColumnNameSanitizer = sqlColumnNameSanitizer;
    }

    public List<TableHeader> convert(XSSFRow headers) {
        return IntStream.range(0, headers.getLastCellNum())
                .mapToObj(headers::getCell)
                .map(XSSFCell::getStringCellValue)
                .map(sqlColumnNameSanitizer::sanitize)
                .map(title -> TableHeader.builder()
                        .title(title)
                        .build())
                .collect(Collectors.toList());
    }
}
