package mercuryds.helium.tableloader;

import com.google.inject.Exposed;
import com.google.inject.PrivateModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JOOQConnectionModule extends PrivateModule {
    @Override
    protected void configure() {

    }

    @Exposed
    @Provides
    public Connection get(@Named("db.userName") String userName,
                          @Named("db.password") String password,
                          @Named("db.connectUrl") String connectUrl) {
        try {
            return DriverManager.getConnection(connectUrl, userName, password);
        } catch (SQLException e) {
            throw new RuntimeException("Error creating sql connection", e);
        }
    }
}
