package mercuryds.helium.tableloader;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import lombok.extern.slf4j.Slf4j;
import mercuryds.helium.data.ColumnInfo;
import mercuryds.helium.data.TableData;
import mercuryds.helium.data.TableHeader;
import org.jooq.*;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.table;

@Slf4j
public class TableToSQLWriter {
    private final Provider<Connection> connection;
    private final SQLDialect mysql;

    @Inject
    public TableToSQLWriter(Provider<Connection> connection,
                            @Named("db.dialect") SQLDialect mysql) {
        this.connection = connection;
        this.mysql = mysql;
    }

    public void write(TableData tableData) {
        log.info("Loading table {}", tableData.getTableName());
        try (Connection c = connection.get()) {
            DSLContext context = DSL.using(c, mysql);
            context.dropTableIfExists(tableData.getTableName()).execute();

            CreateTableAsStep<Record> table = context.createTable(tableData.getTableName());

            Iterator<ColumnInfo> columnInfoIterator = tableData.getColumnInfo().iterator();
            Iterator<TableHeader> headersIterator = tableData.getHeaders().iterator();
            CreateTableColumnStep column = null;

            AtomicInteger columnCount = new AtomicInteger(0);
            while(headersIterator.hasNext() && columnInfoIterator.hasNext()) {
                columnCount.incrementAndGet();
                TableHeader next = headersIterator.next();
                ColumnInfo columnInfo = columnInfoIterator.next();
                log.info("Loading {}, {}", columnInfo, next);
                if (column == null) {
                    column = table.column(next.getTitle(), columnInfo.getJooqType());
                } else {
                    column = column.column(next.getTitle(), columnInfo.getJooqType());
                }
            }
            if(column == null)
                throw new RuntimeException("Trying to create a table with no columns");
            column.execute();

            context.batch
                    (
                            tableData.getData().stream()
                                    .map(data -> context.insertInto(table(tableData.getTableName()))
                                            .values(data.subList(0, columnCount.get())))
                                    .collect(Collectors.toList())
                    ).execute();
        } catch (SQLException e) {
            log.error("Error while writing table", e);
        }
    }
}
