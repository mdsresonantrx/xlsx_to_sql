package mercuryds.helium.typechecker;

import mercuryds.helium.data.ColumnInfo;

/**
 * Created by bachman on 12/21/2017.
 */
public interface TypeChecker {
    boolean allows(Object string);
    ColumnInfo getColumnInfo();
}
