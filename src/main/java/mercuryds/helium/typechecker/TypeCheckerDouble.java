package mercuryds.helium.typechecker;

import mercuryds.helium.data.ColumnInfo;
import org.jooq.impl.SQLDataType;

public class TypeCheckerDouble implements TypeChecker {
    @Override
    public boolean allows(Object value) {
        return (value instanceof Double);

    }

    @Override
    public ColumnInfo getColumnInfo() {
        return ColumnInfo.builder()
//                .properties("varchar(200)")
                .jooqType(SQLDataType.DECIMAL(10))
                .build();

    }
}
