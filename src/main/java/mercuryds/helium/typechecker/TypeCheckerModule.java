package mercuryds.helium.typechecker;

import com.google.inject.PrivateModule;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;

import java.util.Set;

public class TypeCheckerModule extends PrivateModule {
    @Override
    protected void configure() {
        Multibinder<TypeChecker> uriBinder = Multibinder.newSetBinder(binder(), TypeChecker.class);
        uriBinder.addBinding().to(TypeCheckerDouble.class);
        uriBinder.addBinding().to(TypeCheckerString.class);

        expose(new TypeLiteral<Set<TypeChecker>>() {});
    }
}
