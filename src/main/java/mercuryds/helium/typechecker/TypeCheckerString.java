package mercuryds.helium.typechecker;

import mercuryds.helium.data.ColumnInfo;
import org.jooq.impl.SQLDataType;

/**
 * Created by bachman on 12/21/2017.
 */
public class TypeCheckerString implements TypeChecker {
    @Override
    public boolean allows(Object string) {
        return true;
    }

    @Override
    public ColumnInfo getColumnInfo() {
        return ColumnInfo.builder()
                .properties("varchar(200)")
                .jooqType(SQLDataType.VARCHAR.length(200))
                .build();
    }
}
